define([
  'angular',
  './endpoints.service'
], function (angular, endpointsService) {
  'use strict';

  angular
    .module('endpointsModule',[])
    .service('endpointsService', endpointsService);

});
