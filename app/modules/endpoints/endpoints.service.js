define([
  'async!https://apis.google.com/js/client.js!onload'
], function() {
  'use strict';

  endpointsService.$inject = ['$q'];

  function endpointsService($q){
    var service = this;

    var apis = {
      customer: {name: 'customer', version: 'v1', url: 'http://localhost:8080/_ah/api'}
    };

    service.execute = execute;

    /////////
    function execute(apiName, method, params){
      var deferred = $q.defer();
      load(apiName).then(function() {
        var pathMethod = method.split('.');
        var api = gapi.client[apiName];

        for(var i= 0; i < pathMethod.length; i++) {
           api = api[pathMethod[i]];
        }
        api(params).then(function(response) {
          deferred.resolve(response);
        }, function(response){
          deferred.reject(response);
        });
      }, function(response){
        deferred.reject(response);
      });
      return deferred.promise;
    }

    function load(apiName){
      var deferred = $q.defer();

      if(apis[apiName]){
        var apiToLoad = apis[apiName];
        if(apiToLoad.loaded){
          deferred.resolve();
        }else{
          gapi.client.load(apiToLoad.name,
            apiToLoad.version,
            function(){
              markLoaded(apiToLoad, deferred);
            },
            apiToLoad.url);
        }
      }else{
        deferred.reject("ERROR: No config set for " + apiName + " API.");
      }

      return deferred.promise;
    }

    function markLoaded(apiToLoad, deferred){
      apiToLoad.loaded = true;
      deferred.resolve();
    }
  }

  return endpointsService;
});
