define([
], function(){
  'use strict'

  customerEndpointService.$inject = ['$q', 'endpointService'];

  function customerEndpointService($q, endpointService){

    var vm = this;
    vm.getAllCustomers = getAllCustomers;
    vm.addCustomer = addCustomer;
    vm.deleteCustomer = deleteCustomer;
    vm.deleteSelectedCustomer = deleteSelectedCustomer;
    vm.apiName = 'customer';

    function getAllCustomers(){
      var p = $q.defer();

      endpointService.execute(vm.apiName, 'getAll')
        .then(function(response){
            p.resolve(response);
            console.log("Success " + response);
        }, function(response){
            console.log("Failed " + response);
            p.reject("Service Unavailable");
        });
      return p.promise;
    }

    function getCustomer(user_id){
      var p = $q.defer();

      endpointService.execute(vm.apiName, 'get', user_id)
        .then(function(response){
            console.log("Success " + response);
            p.resolve(response);
        }, function(response){
            console.log("Failed " + response);
            p.reject("Service Unavailable");
        });
      return p.promise;
    }

    function deleteCustomer(customer){
      var p = $q.defer();
      endpointService.execute(vm.apiName, 'delete', customer)
        .then(function(response){
            p.resolve(response);
            console.log("Success " + response);
        }, function(response){
            console.log("Failed " + response);
            p.reject("Service Unavailable");
        });
      return p.promise;
    }

    function deleteSelectedCustomer(customers){
      var p = $q.defer();

      endpointService.execute(vm.apiName, 'deleteSelected', customers)
        .then(function(response){
            p.resolve(response);
            console.log("Success " + response);
        }, function(response){
            console.log("Failed " + response);
            p.reject("Service Unavailable");
        });
      return p.promise;
    }

    function addCustomer(entity){
      var p = $q.defer();

      console.log(JSON.stringify(entity));
      endpointService.execute(vm.apiName, 'add', entity)
        .then(function(response){
            p.resolve(entity.customerName);
            console.log("Success " + response);
        }, function(response){
            console.log("Failed " + response);
            p.reject("Service Unavailable");
        });
      return p.promise;
    }
  }

  return customerEndpointService;
})