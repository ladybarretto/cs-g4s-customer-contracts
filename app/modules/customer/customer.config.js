define(['require'], function (require) {
  config.$inject = ['$routeProvider', '$mdThemingProvider'];

  function config($routeProvider, $mdThemingProvider) {
    $routeProvider.when('/customer', {
      templateUrl: require.toUrl('./customer.html'),
      controller: 'CustomerController',
      controllerAs: 'cc',
      loginRequired: true,
      resolve: {
        details: ['customerService', function (customerService) {
          var result = customerService.getAllCustomers();
          return result;
        }]
      }
    });

    $mdThemingProvider.theme('default')
    .primaryPalette('blue', {
     'default': '800', // by default use shade 400 from the pink palette for primary intentions
     'hue-1': '700', // use shade 100 for the <code>md-hue-1</code> class
     'hue-2': '900', // use shade 600 for the <code>md-hue-2</code> class
    })
    .accentPalette('grey', {
      'default': '400'
    });

  }

  return config;
});
