define([], function () {
  'use strict';

  customerController.$inject = ['$scope', 'details','$timeout', '$mdDialog', 'customerService'];

  function customerController($scope, details, $timeout, $mdDialog, customerService) {
    var vm = this;
    vm.ready = false;
    vm.setActive = setActive;
    vm.addCustomer = addCustomer;
    vm.getCustomers = getCustomers;
    vm.removeCustomer = removeCustomer;
    vm.removeSelected = removeSelected;
    vm.removeItem = removeItem;
    vm.editCustomer = editCustomer;
    vm.viewCustomer = viewCustomer;
    vm.toggle = toggle;
    vm.exists = exists;
    vm.disabled = disabled;
    vm.items = details.result.items;
    vm.totalItems = vm.items.length;

    activate();

    function activate(){
      disableCheckbox();
    }

    function disableCheckbox(){
      vm.checkboxSelected = [];
      vm.numSelected = vm.checkboxSelected.length
    }

    function toggle(customer){
      var index = vm.checkboxSelected.indexOf(customer);
      if (index > -1) vm.checkboxSelected.splice(index, 1);
      else vm.checkboxSelected.push(customer);

      vm.numSelected = vm.checkboxSelected.length;
    }

    function disabled(){
      return vm.numSelected < 1
    }
    function exists(customer){
      return vm.checkboxSelected.indexOf(customer) > -1;
    }

    function setActive(index, data) {
      vm.ready = false;
      $timeout(function () {
        vm.selected = data;
        vm.currentIndex = index;
        vm.ready = true;
      }, 100);
    }

    function getCustomers(){
      return customerService.getAllCustomers().then(function(response){
        vm.items = response.result.items;
        vm.totalItems = vm.items.length;
        return response;
      }, function(error){
        return error;
      });
    }

    function removeSelected(ev){
      var confirm = $mdDialog.confirm()
          .title('Are you sure you want to delete the selected customer/s?')
          .content('All contracts of each customer will be deleted also.')
          .ariaLabel('Lucky day')
          .targetEvent(ev)
          .ok('Delete')
          .cancel('Cancel');
      $mdDialog.show(confirm).then(function() {
        vm.removeItem(vm.checkboxSelected);
        disableCheckbox();
        $scope.status = 'Successfully deleted selected Customers';
        console.log($scope.status);
      }, function() {
        $scope.status = 'Cancelled deletion of selected customers';
        console.log($scope.status);
      });
    }

    function removeCustomer(ev, row){
      var confirm = $mdDialog.confirm()
          .title('Are you sure you want to delete the selected customer, ' + row.customerName + '?')
          .content('All contracts of ' + row.customerName + ' will be deleted also.')
          .ariaLabel('Lucky day')
          .targetEvent(ev)
          .ok('Delete')
          .cancel('Cancel');
      $mdDialog.show(confirm).then(function() {
        vm.removeItem(row);
        $scope.status = 'Successfully deleted selected Customers';
        console.log($scope.status);
      }, function() {
        $scope.status = 'Cancelled deletion of selected customers';
        console.log($scope.status);
      });
    }

    function removeFromList(data){
      var index = vm.items.indexOf(data);
      if (index !== -1) {
          vm.items.splice(index, 1);
      }
    }

    function removeItem(data){
      var customers = {"items": data}
      if (data instanceof Array){
        for (var i = 0; i < data.length; i++){
          removeFromList(data[i]);
          vm.totalItems = vm.items.length;
        }
        return customerService.removeSelectedCustomer(customers).then(function(response){
          vm.checkboxSelected = [];
          return response;
        }, function(error){
          return error;
        });

      } else {
        removeFromList(data);
        vm.totalItems = vm.items.length;
        return customerService.removeCustomer(data).then(function(response){
        vm.checkboxSelected = [];
          return response;
        }, function(error){
          return error;
        });
      }
      
    }

    function addCustomer(ev){
      $mdDialog.show({
        controller: 'AddDialogController',
        templateUrl: 'modules/customer/customer.add.html',
        clickOutsideToClose: true,
        targetEvent: ev,
        parent: angular.element(document.body)
      })
      .then(function(answer){
        vm.getCustomers();
      }, function(){
        console.log('Adding New Customer Cancelled..');
      });
    }

    function editCustomer(data){
      $mdDialog.show({
        templateUrl: 'modules/customer/customer.edit.html',
        clickOutsideToClose: false,
        parent: angular.element(document.body),
        locals: {
          item: data
        },
        controller: 'EditDialogController'
      })
      .then(function(answer){
        console.log('Successfully Updated new customer, ' + answer);
        vm.getCustomers();
      }, function(){
        console.log('Updating New Customer Cancelled..');
      });
    }

    function viewCustomer(data){
      $mdDialog.show({
        templateUrl: 'modules/customer/customer.view.html',
        clickOutsideToClose: false,
        parent: angular.element(document.body),
        locals: {
          item: data
        },
        controller: 'ViewDialogController',
        controllerAs: 'vc'
      })
      .then(function(answer){
        console.log('Viewed current customer, ' + answer);
      }, function(){
        console.log('Viewing Done..');
      });
    }

  }

  return customerController;
});
