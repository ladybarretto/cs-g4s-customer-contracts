define([
  'customer/customer.config',
  'customer/customer.controller',
  'customer/customer.service',
  'convertDate-filter',
  'customerEndpointService',
  'endpointService',
  'addDialogController',
  'editDialogController',
  'viewDialogController',
  'angular',
  'angular-route',
  'angular-bootstrap',
  'smart-table',
  'angular-material',
  'angular-messages'], function (config, controller, service, convertDateFilter, customerEndpointService, endpointService, addDialogController, editDialogController, viewDialogController){
  angular.module('customerModule', ['ngRoute', 'smart-table', 'ngMaterial', 'ngMessages'])
    .controller('CustomerController', controller)
    .controller('AddDialogController', addDialogController)
    .controller('EditDialogController', editDialogController)
    .controller('ViewDialogController', viewDialogController)
    .service('customerService', service)
    .filter('convertDate', convertDateFilter)
    .service('customerEndpointService', customerEndpointService)
    .service('endpointService', endpointService)
    .config(config);
});