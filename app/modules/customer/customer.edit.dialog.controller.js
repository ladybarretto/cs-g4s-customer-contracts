define([], function () {
  'use strict';

  EditDialogController.$inject = ['$scope', '$mdDialog', 'item', 'customerService'];

  function EditDialogController($scope, $mdDialog, customer, customerService) {
    $scope.closeDialog = closeDialog;
    $scope.editCustomer = editCustomer;
    $scope.dummyContracts = ['Contract#1231', 'Contract#1232', 'Contract#1233', 'Contract#1234', 'Contract#1235', 'Contract#1236'];
    $scope.customer = {
      customerName: customer.customerName,
      description: customer.description,
      emailAddress: customer.emailAddress,
      phoneNumber: customer.phoneNumber,
      id: customer.id,
      contracts: $scope.dummyContracts
    };

    function closeDialog(){
      $mdDialog.cancel();
    }

    function viewNotif(customerName){
     $mdDialog.show(
        $mdDialog.alert()
          .parent(angular.element(document.querySelector('#popupContainer')))
          .clickOutsideToClose(true)
          .title('You Successfully Updated customer, ' + customerName)
          .content('You can now add new contracts for ' + customerName)
          .ariaLabel('Add Confirmation')
          .ok('Okay')
      );
    }

    function editCustomer(){
      // // User service / factory
      customerService.addCustomer($scope.customer);
      viewNotif($scope.customer.customerName);
    }
  }

  return EditDialogController;
});
