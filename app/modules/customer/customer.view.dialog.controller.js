define([], function () {
  'use strict';

  ViewDialogController.$inject = ['$scope', '$mdDialog', 'item'];

  function ViewDialogController($scope, $mdDialog, item) {
    $scope.closeDialog = closeDialog;
    $scope.dummyContracts = ['Contract#1231', 'Contract#1232', 'Contract#1233', 'Contract#1234', 'Contract#1235', 'Contract#1236'];
    $scope.customer = {
      customerName: item.customerName,
      description: item.description,
      emailAddress: item.emailAddress,
      phoneNumber: item.phoneNumber,
      dateAdded: item.dateAdded,
      contracts: $scope.dummyContracts
    };

    function closeDialog(){
      $mdDialog.hide($scope.customer.customerName);
    }
  }

  return ViewDialogController;
});
