define(['moment'], function (moment) {
  'use strict';

  AddDialogController.$inject = ['$scope', '$mdDialog', 'customerService'];

  function AddDialogController($scope, $mdDialog, customerService) {
    $scope.closeDialog = closeDialog;
    $scope.addCustomer = addCustomer;
    $scope.customer = {
      customerName: '',
      description: '',
      emailAddress: '',
      phoneNumber: ''
    };

    function closeDialog(){
      $mdDialog.hide();
    }

    function addNotif(customerName){
     $mdDialog.show(
        $mdDialog.alert()
          .parent(angular.element(document.querySelector('#popupContainer')))
          .clickOutsideToClose(true)
          .title('You Successfully added a new customer, ' + customerName)
          .content('You can now add new contracts for ' + customerName)
          .ariaLabel('Add Confirmation')
          .ok('Okay')
      );
    }

    function addCustomer(){
      if($scope.customer.customerName && $scope.customer.emailAddress){
        // User service / factory
        customerService.addCustomer($scope.customer);
        addNotif($scope.customer.customerName);
      }
    }

  }

  return AddDialogController;
});
