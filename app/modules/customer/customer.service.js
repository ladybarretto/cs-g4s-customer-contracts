define([], function () {
  'use strict';


  var customerService = function service($http, customerEndpointService) {

    var vm = this;
    vm.getAllCustomers = getAllCustomers;
    vm.addCustomer = addCustomer;
    vm.removeCustomer = removeCustomer;
    vm.removeSelectedCustomer = removeSelectedCustomer;

    function getAllCustomers() {
      return customerEndpointService.getAllCustomers().then(function (response) {
        return response;
      }, function(error) {
        return error;
      });
    }

    function addCustomer(customer){
      return customerEndpointService.addCustomer(customer).then(function (response) {
        return response;
      }, function (error){
        return error;
      });
    }

    function removeCustomer(customer){
      return customerEndpointService.deleteCustomer(customer).then(function (response) {
        return response;
      }, function(error){
        return error;
      });
    }

    function removeSelectedCustomer(customer){
      return customerEndpointService.deleteSelectedCustomer(customer).then(function (response) {
        return response;
      }, function(error){
        return error;
      });
    }

  };

  customerService.$inject = ['$http', 'customerEndpointService'];

  return customerService;
});