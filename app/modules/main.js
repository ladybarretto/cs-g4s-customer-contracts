require.config({
  paths: {
    'angular':'../../bower_components/angular/angular.min',
    'angular-route': '../../bower_components/angular-route/angular-route',
    'angular-bootstrap': '../../bower_components/angular-bootstrap/ui-bootstrap-tpls',
    'angular-material': '../../bower_components/angular-material/angular-material.min',
    'angular-aria': '../../bower_components/angular-aria/angular-aria.min',
    'angular-animate': '../../bower_components/angular-animate/angular-animate.min',
    'angular-messages': '../../bower_components/angular-messages/angular-messages.min',
    bootstrap: '../../bower_components/bootstrap/dist/js/bootstrap.min',
    bootstrapTable: '../../bower_components/bootstrap-table/dist/js/bootstrap-table.min',
    endpointService: 'endpoints/endpoints.service',
    'customerEndpointService': 'customer/customer.endpoint.service',
    jquery: '../../bower_components/jquery/dist/jquery.min',
    'convertDate-filter': 'common/convertDate.filter',
    addDialogController: 'customer/customer.add.dialog.controller',
    editDialogController: 'customer/customer.edit.dialog.controller',
    viewDialogController: 'customer/customer.view.dialog.controller',
    moment: '../../bower_components/moment/moment',
    'smart-table': '../../bower_components/angular-smart-table/dist/smart-table.min',
    'angular-google-gapi': '../../bower_components/angular-google-gapi/angular-google-gapi.min',
    async: '../../bower_components/requirejs-plugins/src/async'
   },
  shim: {
    bootstrap: {
      deps: [
        'jquery'
      ]
    },
    bootstrapTable: {
      deps: [
        'jquery'
      ]
    },
    'smart-table': [
      'angular'
    ],
    'angular-animate': [
      'angular'
    ],
    'angular-aria': [
      'angular'
    ],
    'angular-material': [
      'angular-aria',
      'angular-animate'
    ],
    angular: {
      exports: 'angular'
    },
    'angular-route': [
      'angular'
    ],
    'angular-bootstrap': [
      'angular'
    ],
    'angular-messages': [
      'angular'
    ]
  },
  packages: [],
  waitSeconds: 60
});
require([
  'angular',
  'bootstrap',
  'angular-material',
  'customer/customer.module',
  'navigation/navigation.module',
  'common/common.module'
], function () {
  'use strict';

  angular.bootstrap(document,['customerModule', 'navigationModule']);

});
