define([
'angular',
'convertDate-filter'
], function(
    angular,
    convertDateFilter){
  angular.module('commonModule', [])
      .filter('convertDate', convertDateFilter)
});
