define([], function () {
  'use strict';

  config.$inject = ['$routeProvider', '$locationProvider'];

  function config($routeProvider, $locationProvider) {
    $routeProvider.otherwise({
      redirectTo: '/customer'
    });
    $locationProvider.html5Mode(true);
  }

  return config;
});
