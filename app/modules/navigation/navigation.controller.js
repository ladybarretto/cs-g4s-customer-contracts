define([], function () {
  'use strict';

  controller.$inject = ['$window', '$location'];

  function controller($window, $location) {
    var vm = this;
    vm.activeTab = $location.path();
    vm.changeTab = changeTab;

    function changeTab(tabName) {
      vm.activeTab = tabName;
    }

  }

  return controller;
});