# Secure - Customers #

Customer Module of G4S - Secure

## Requirements ##
* Grunt CLI
* NodeJS
* Bower

## How to initialize: ##

    $ npm install
    $ bower install

## How to run: ##
    $ grunt serve# fullstackproject
